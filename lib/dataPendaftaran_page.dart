import 'package:flutter/material.dart';
import './miscelanous.dart';

class DataPendaftaran extends StatefulWidget {
  DataPendaftaran({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _DataPendaftaranState createState() => _DataPendaftaranState();
}

class _DataPendaftaranState extends State<DataPendaftaran> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBarUtama(title: widget.title,),
      drawer: DrawerUtama(),
      backgroundColor: Colors.grey[100],
      body: NoData()
    );
  }
}