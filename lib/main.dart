import 'package:flutter/material.dart';
import './mitra_page.dart';
import './dataPendaftaran_page.dart';
import './miscelanous.dart';
import './lowongan_page.dart';
import './profile_page.dart';
import './menu/setting.dart';
import './gallery.dart';
import './search.dart';
import 'package:material_search/material_search.dart';

final GlobalKey<ScaffoldState> _scaffoldState = GlobalKey<ScaffoldState>();

String lowonganKerja = 'Lowongan Kerja';
String dataPendaftaran = 'Data Pendaftaran';
String mitraBKK = 'Mitra BKK';
String profile = 'Profile';
String setting = 'Setting';
String gallery = 'Gallery';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'BKK SMK SORE TULUNGAGUNG',
      home: new MyHomePage(),
      // theme: ThemeData(fontFamily: 'Montserrat'),
      routes: <String, WidgetBuilder>{
        '/search' : (BuildContext context) => SearchBar(),
        '/homepage' : (BuildContext context) => MyHomePage(),
        '/MitraBKK' : (BuildContext context) => MitraBKK(title: mitraBKK,),
        '/DataPendaftaran' : (BuildContext context) => DataPendaftaran(title: dataPendaftaran,),
        '/lowonganKerja' : (BuildContext context) => LowonganKerja(title: lowonganKerja,),
        '/profile' : (BuildContext context) => ProfilePage(title: profile,),
        '/setting' : (BuildContext context) => SettingMenu(title: setting,),
        '/gallery' : (BuildContext context) => Gallery(title: gallery,),
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => new _MyHomePageState();
}


class _MyHomePageState extends State<MyHomePage> {
    
  final _title=[
    '$mitraBKK',
    '$dataPendaftaran',
    '$lowonganKerja',
    '$profile',
    '$setting',
    '$gallery',
  ];

  String _name = 'No one';

  // final _formKey = new GlobalKey<FormState>();
  _buildMaterialSearchPage(BuildContext context) {
    return new MaterialPageRoute<String>(
      settings: new RouteSettings(
        name: 'material_search',
        isInitialRoute: false,
      ),
      builder: (BuildContext context) {
        return new Material(
          child: new MaterialSearch<String>(
            placeholder: 'Search',
            results: _title.map((String value) => new MaterialSearchResult<String>(
              // icon: Icons.person,
              value: value,
              text: "$value",
            )).toList(),
            filter: (dynamic value, String criteria) {
              return value.toLowerCase().trim()
                .contains(new RegExp(r'' + criteria.toLowerCase().trim() + ''));
            },
            // onSelect: (dynamic value){
            //   Navigator.of(context).push(value);
            // },
            onSelect: (dynamic value) => Navigator.of(context).pop(value),
            onSubmit: (String value) => Navigator.of(context).pop(value),
          ),
        );
      }
    );
  }

  _searchPindah(){
    setState(() {
      _buildMaterialSearchPage(context);
    });
  }
  _showMaterialSearch(BuildContext context) {
    Navigator.of(context)
      .push(_buildMaterialSearchPage(context))
      .then((dynamic value) {
        setState(() => _name = value as String);
      });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: _scaffoldState,
      drawer: DrawerUtama(),
      backgroundColor: Colors.blueGrey[50],
      body: WillPopScope(
              child: Container(
                child: Stack(
          children: <Widget>[
            Container(
                padding: EdgeInsets.only(top: 25.0,left: 5.0,right: 5.0),
                height: 335.0,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    colors: [
                      // Colors.lightBlueAccent[700],
                      Colors.indigoAccent[400],
                      Colors.cyanAccent[400],
                    ]
                  )
                ),
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Material(
                          color: Colors.transparent,
                          child: IconButton(
                            splashColor: Colors.white30,
                            highlightColor: Colors.white10,
                            icon: Icon(Icons.menu, size: 25.0,),
                            onPressed: () {
                              _scaffoldState.currentState.openDrawer();
                              // Scaffold.of(context).openDrawer();
                              },
                            color: Colors.white,
                          ),
                        ),
                        Expanded(
                          child: Text(
                            'BKK SMK SORE\nTULUNGAGUNG',
                            style: TextStyle(color: Colors.white, fontSize: 18.0),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        Material(
                          color: Colors.transparent,
                          child: IconButton(
                            splashColor: Colors.white30,
                            highlightColor: Colors.white10,
                            icon: Icon(Icons.search, size: 25.0,),
                            onPressed: () {
                              _showMaterialSearch(context);
                              // Navigator.pushNamed(context, '/search');
                            },
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.all(5.0),
                        ),
                            CircleAvatar(
                              radius: 60,
                              backgroundColor: Colors.white10,
                              backgroundImage: AssetImage(
                                "assets/img/logo.png",
                              ),
                            ),
                        Container(
                          padding: EdgeInsets.all(10.0),
                          child: Text(
                            "SMK SORE TULUNGAGUNG\nSekolah swasta dengan BKK terbaik\ndi Jawa Timur",
                            style: TextStyle(color: Colors.white,fontSize: 17.0),
                            textAlign: TextAlign.center,
                          ),
                        )
                      ],
                    )
                  ],
                )
            ),
            Positioned(
                child: Container(
                  height: 660.0,
                  padding: EdgeInsets.all(13.0),
                  child: Card(
                    elevation: 2.0,
                    margin: EdgeInsets.only(top: 275.0),
                    child: GridView.count(
                      crossAxisCount: 2,
                      padding: EdgeInsets.all(7.0,),
                      children: <Widget>[
                        HomeMenu(
                          pindah: '/MitraBKK',
                          image: "assets/img/mitra.png",
                          text: "Mitra BKK",
                        ),
                        HomeMenu(
                          pindah: '/lowonganKerja',
                          image: "assets/img/lowongan.png",
                          text: "Lowongan Kerja",
                        ),
                        HomeMenu(
                          pindah: '/gallery',
                          image: "assets/img/gallery.png",
                          text: "Galley BKK",
                        ),
                        HomeMenu(
                          pindah: '/DataPendaftaran',
                          image: "assets/img/data.png",
                          text: "Data Pendaftaran",
                        ),
                      ],
                    ),
                  ),
                )
            ),
            Positioned(
                height: 35.0,
                top: 272.0,
                left: 105,
                right: 105,            
                child: RaisedButton(
                  onPressed: (){
                    Navigator.pushNamed(context, "/profile");
                  },
                  // splashColor: Colors.lightBlue[100],
                  highlightColor: Colors.white,
                  color: Colors.white,
                  child: Text("LIHAT PROFIL", style: TextStyle(fontSize: 17,color: Colors.blue),),
                  ),
            ),
          ],
        ),
              ),
      onWillPop: (){
        return Future.value(false);
      },
      ),
    );
  }
}

class HomeMenu extends StatelessWidget {
  HomeMenu({this.image,this.pindah, this.text});
  final String pindah;
  final String text;
  final String image;
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 0.0,
      child: InkWell(
        onTap: (){
          Navigator.pushNamed(context, pindah);
        },
        child: Container(
          padding: EdgeInsets.all(10.0),
          child: Column(
            children: <Widget>[
              Expanded(child: Image.asset(image, scale: 6.0,)),
              Text(
                text,
                style: TextStyle(fontSize: 13.5),
                textAlign: TextAlign.center,
              )
            ],
          ),
        ),
      ),
    );
  }
}