import 'package:flutter/material.dart';
import 'dart:io';
// import 'package:material_search/material_search.dart';

class AppBarUtama extends StatelessWidget implements PreferredSizeWidget{
  AppBarUtama({this.title});
  final String title;

  @override
  Widget build(BuildContext context) {
    return AppBar(
        backgroundColor: Colors.white,
        title: new Text(
          title.toUpperCase(),
          style: TextStyle(color: Colors.lightBlueAccent[700]),
        ),
        leading: IconButton(
          onPressed: ()=>Navigator.pop(context),
          icon: Icon(Icons.chevron_left,size: 30.0,),
          color: Colors.blueAccent[400],
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.search),
            color: Colors.lightBlue[400],
            onPressed: (){},
          ),
        ],
        elevation: 1.0,
        centerTitle: true,
        automaticallyImplyLeading: false,
        bottom: PreferredSize(
          preferredSize: const Size.fromHeight(1.0),
          child: Container(
            // color: Colors.lightBlue[400],
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.centerLeft,
                    end: Alignment.centerRight,
                    colors: [
                      Colors.indigoAccent[400],
                      Colors.cyanAccent[400],
                ])),
            padding: EdgeInsets.only(bottom: 3.0),
          ),
        ),
      );
  }@override
  Size get preferredSize => new Size.fromHeight(kToolbarHeight);
}

class DrawerUtama extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
      void pindahPage(String hal){
    Navigator.pop(context);
    Navigator.pushNamed(context, hal);
  }

    return Drawer(
        child: ListView(
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(15.0),
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: [
                    Colors.indigoAccent[400],
                    Colors.lightBlueAccent[200],
                  ]
                )
              ),
              child: InkWell(
              onTap: (){
                pindahPage('/profile');
                },
                              child: Row(
                  children: <Widget>[
                    CircleAvatar(
                      radius: 30.0,
                      backgroundColor: Colors.transparent,
                      backgroundImage: AssetImage("assets/img/logo.png"),
                    ),
                    Padding(
                      padding: EdgeInsets.all(5.0),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text("BKK SMK SORE TULUNGAGUNG",style: TextStyle(color: Colors.white,fontSize: 16.0,fontWeight: FontWeight.w500),),
                        Padding(padding: EdgeInsets.all(0.8),),
                        Text("www.smksoretulungagung.sch.id",style: TextStyle(color: Colors.white,fontSize: 15.8,fontWeight: FontWeight.w300),),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            // UserAccountsDrawerHeader(
              
            //   accountName: Text("BKK SMK SORE TULUNGAGUNG"),
            //   accountEmail: Text("www.smksoretulungagung.sch.id"),
            //   currentAccountPicture: CircleAvatar(
            //     backgroundColor: Colors.transparent,
            //     backgroundImage: AssetImage("assets/img/logo.png"),
            //   ),
            // ),
            ListTile(
              onTap: (){
                // Navigator.pop(context);
                  Navigator.popUntil(context, ModalRoute.withName(Navigator.defaultRouteName));
                // Navigator.pushNamed(context, "/homepage");
              },
              title: Text("Home"),
              leading: Icon(Icons.home),
            ),
            ListTile(
              onTap: (){
                pindahPage("/MitraBKK");
              },
              title: Text("Mitra BKK"),
              leading: Icon(Icons.people),
            ),
            ListTile(
              onTap: (){
                pindahPage("/lowonganKerja");},
              title: Text("Lowongan Kerja"),
              leading: Icon(Icons.receipt),
            ),
            ListTile(
              onTap: (){
                pindahPage('/gallery');
              },
              title: Text("Gallery BKK"),
              leading: Icon(Icons.photo_size_select_actual),
            ),
            ListTile(
              onTap: (){
                pindahPage("/DataPendaftaran");
              },
              title: Text("Data Pendaftaran"),
              leading: Icon(Icons.featured_play_list),
            ),
            Divider(),
            ListTile(
              onTap: (){
                pindahPage("/setting");
                },
              title: Text("Setting"),
              leading: Icon(Icons.settings),
            ),
            ListTile(
              onTap: (){
                pindahPage('/gallery');
              },
              title: Text("About Us"),
              leading: Icon(Icons.info),
            ),
            ListTile(
              onTap: (){},
              title: Text("Contact Us"),
              leading: Icon(Icons.phone),
            ),
            ListTile(
              onTap: (){},
              title: Text("Beri Feedback"),
              leading: Icon(Icons.feedback),
            ),
            ListTile(
              onTap: ()=>exit(0),
              title: Text("Exit"),
              leading: Icon(Icons.exit_to_app),
            ),
          ],
        ),
      );
  }
}

class NoData extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(5.0),
        child: Column(
          children: <Widget>[
            Padding(padding: EdgeInsets.all(25.0),),
            Center(
              child: Image.asset('assets/img/nodata.png'),
            ),
            Column(
              children: <Widget>[
                Text('Oops!!',style: TextStyle(fontSize: 25.0,fontWeight: FontWeight.w500,color: Colors.lightBlueAccent[700]),),
                Padding(padding: EdgeInsets.all(3.0),),
                Text('Untuk sementara masih belum ada data disini..',style: TextStyle(fontSize: 18.0,color: Colors.lightBlueAccent[700],fontWeight: FontWeight.w300),)
              ],
            ),
          ],
        ),
      );
  }
}
