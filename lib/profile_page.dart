import 'package:flutter/material.dart';
import './miscelanous.dart';

class ProfilePage extends StatefulWidget {
  ProfilePage({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBarUtama(title: widget.title,),
      drawer: DrawerUtama(),
      backgroundColor: Colors.grey[100],
      body: ListView(
        padding: EdgeInsets.all(5.0),
        children: <Widget>[
        ],
      ),
    );
  }
}