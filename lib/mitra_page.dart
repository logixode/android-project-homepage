import 'package:flutter/material.dart';
import './miscelanous.dart';

class MitraBKK extends StatelessWidget {
  MitraBKK({this.title});
  final String title;

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBarUtama(title: title,),
      drawer: DrawerUtama(),
      backgroundColor: Colors.grey[100],
      body: GridView.count(
        padding: EdgeInsets.all(5.0),
        crossAxisCount: 2,
        children: <Widget>[
          HomeCard(
            logo: "assets/img/jiaec.png",
            namaPerusahaan: "PT JIAEC",
          ),
          HomeCard(
            logo: "assets/img/pt-medion.jpg",
            namaPerusahaan: "PT MEDION",
          ),
          HomeCard(
            logo: "assets/img/pt-pama.png",
            namaPerusahaan: "PT PAMA",
          ),
          HomeCard(
            logo: "assets/img/cipta-futura.png",
            namaPerusahaan: "PT CIPTA FUTURA",
          ),
          HomeCard(
            logo: "assets/img/jiaec.png",
            namaPerusahaan: "PT JIAEC",
          ),
          HomeCard(
            logo: "assets/img/jiaec.png",
            namaPerusahaan: "PT JIAEC",
          ),
        ],
      ),
    );
  }
}

class HomeCard extends StatelessWidget {
  HomeCard({this.logo, this.namaPerusahaan});
  final String logo;
  final String namaPerusahaan;
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(3.0),
      child: Card(
        elevation: 2.0,
        child: InkWell(
          onTap: () {},
          child: Padding(
            padding: EdgeInsets.all(10.0),
            child: Column(
              children: <Widget>[
                Expanded(
                  child: Image.asset(
                    logo,
                    fit: BoxFit.fill,
                    // width: MediaQuery.of(context).size.width,
                  ),
                ),
                Text(namaPerusahaan)
              ],
            ),
          ),
        ),
      ),
    );
  }
}
