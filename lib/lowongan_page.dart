import 'package:flutter/material.dart';
import './miscelanous.dart';

class LowonganKerja extends StatefulWidget {
  LowonganKerja({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _LowonganKerjaState createState() => _LowonganKerjaState();
}

class _LowonganKerjaState extends State<LowonganKerja> {
  // _LowonganKerjaState({this.title});
  // String title;

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBarUtama(title: widget.title),
      drawer: DrawerUtama(),
      backgroundColor: Colors.grey[100],
      body: NoData()
    );
  }
}