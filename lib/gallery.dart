import 'package:flutter/material.dart';
import './miscelanous.dart';
import 'package:zoomable_image/zoomable_image.dart';

class Gallery extends StatefulWidget {
  Gallery({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _GalleryState createState() => _GalleryState();
}

class _GalleryState extends State<Gallery> {
  List<Container> listFoto = List();
  var foto=[
    "portfolio-1.jpg",
    "portfolio-2.jpg",
    "portfolio-3.jpg",
    "portfolio-4.jpg",
    "portfolio-5.jpg",
    "portfolio-6.jpg",
    "portfolio-7.jpg",
    "portfolio-8.jpg",
    "portfolio-9.jpg",
    "portfolio-10.jpg",
    "portfolio-11.jpg",
    "portfolio-12.jpg",
    "portfolio-13.jpg",
    "portfolio-14.jpg",
  ];

  _listFoto()async{
    for (var i = 0; i < foto.length; i++) {
      final fotoList = foto[i];
      listFoto.add(
        Container(
          margin: EdgeInsets.all(2.5),
          child: GestureDetector(
            onTap: ()=>Navigator.of(context).push(MaterialPageRoute(
              builder: (BuildContext context)=>DetailFoto(fotoList: fotoList,)
            )),

            child: Image.asset("assets/img/gallery/$fotoList",fit: BoxFit.cover,)
          ),
        ),
      );
    }
  }

  @override
  void initState() {
    _listFoto();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBarUtama(title: widget.title,),
      drawer: DrawerUtama(),
      backgroundColor: Colors.grey[100],
      body: GridView.count(
        shrinkWrap: true,
        childAspectRatio: (1.0 / 1.0),
        padding: EdgeInsets.all(2.0),
        crossAxisCount: 2,
        children: listFoto,
      ),
    );
  }
}

class DetailFoto extends StatelessWidget {
  DetailFoto({this.fotoList});
  final String fotoList;
  @override
  Widget build(BuildContext context) {
    return ZoomableImage(AssetImage("assets/img/gallery/$fotoList",));
  }
}